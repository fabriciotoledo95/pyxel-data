# Parametric mode: PTC curve

This notebook can be run in the cloud using Binder:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab/tree/examples/parametric/parametric.ipynb)
