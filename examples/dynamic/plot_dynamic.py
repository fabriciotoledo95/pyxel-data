import holoviews as hv
from holoviews import opts
hv.extension('bokeh', 'matplotlib')


class dynamic_results:
    """
    Object to display results of the dynamic mode
    """
    def __init__(self, xarray):
        _ = self.to_hv(xarray)
    
    def to_hv(self, xarray):
        """
        Converts a xarray from the dynamic mode to holoview object
        """
        # Get the xarray dataset generated using the dynamic mode
        data = xarray['image']
        #data[np.where(data<0)] = 0
        # Convert it into a Holoview Dataset
        dataset3d = hv.Dataset((range(data.shape[1]), 
                                range(data.shape[2]), 
                                range(data.shape[0]), data), 
                               ['x', 'y', 't'], 
                               'Image array')
        self.data = dataset3d
        
        return (dataset3d)
    
    def display_exposure(self, clim=(0, 100000)) -> None:
        """
        Display the exposure in dynamic mode as a GriddedArray with a slider for time steps
        """
        # These are options for the plot
        # check: http://holoviews.org/user_guide/Customizing_Plots.html
        opts.defaults(
            opts.GridSpace(shared_xaxis=True, shared_yaxis=True),
            opts.Image(cmap='viridis', width=400, height=400, clim=clim, tools=['hover']),
            opts.Labels(text_color='white', text_font_size='8pt', text_align='left', text_baseline='bottom'),
            opts.Path(color='white'),
            opts.Spread(width=600),
            opts.Overlay(show_legend=True))

        # Display the 3D dataset and add an histogram the dimension left out
        # is used for a slider widget to scan through the dataset
        hvdata = self.data.to(hv.Image, ['x', 'y']).hist()
        return hvdata
        
    def display_roi(self, roi: tuple):
        """
        Select an ROI on the dataset and display the exposure and the pixel by pixel integration
        Input:
        Tuple ((start_x, stop_x, start_y, stop_y))
        Returns:
        roi_display: holoview Gridded Data
        roi_rampdisplay: holoview Gridded pixel /pixel evolution
        """
        # Define Region(s) Of Interest (ROIs)
        ROIs = [roi]
        # Create the rectangles from the ROIs tuples
        roi_bounds = hv.Path([hv.Bounds(roi) for roi in ROIs])
        # Label them
        labels = hv.Labels([(roi[0], roi[1], i) for i, roi in enumerate(ROIs)])
        # Display the first image of the dataset
        # TODO change order to have time dimension first and not have to use ':'
        (self.data[:, :, 0].to(hv.Image, ['x', 'y']) * roi_bounds * labels).relabel(f'Location of ROI: {roi}')
        
        # Take one RoI and display it
        x0, y0, x1, y1 = ROIs[0]
        roi = self.data.select(x=(x0, x1), y=(y0, y1), time=(0, 4)).relabel('ROI #2')
        roi_display = roi.to(hv.Image, ['x', 'y'])
        
        # Plot the pixel by pixel non destructive reads
        roi_rampdisplay = roi.to(hv.Curve, 't').grid()
        
        return roi_display, roi_rampdisplay