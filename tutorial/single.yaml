# ########################################################### #
# Pyxel detector simulation framework                         #
#                                                             #
# Example YAML configuration file                             #
# Single mode combining different detector effect models      #                         
# ########################################################### #


single:

  outputs:
    output_folder: "output"
    save_data_to_file:
      - detector.image.array:   ['fits']
      - detector.pixel.array: ['npy']
      
      
ccd_detector:

  geometry:

    row: 450               # pixel
    col: 450               # pixel
    total_thickness: 40.    # um
    pixel_vert_size: 10.    # um
    pixel_horz_size: 10.    # um

  material:
    material: 'silicon'

  environment:
    temperature: 300        # K

  characteristics:
    qe:   1.                # -
    eta:  1.                # e/photon
    sv:   3.e-6             # V/e
    amp:  10.               # V/V
    a1:   100.              # V/V
    a2:   3000              # DN/V
    fwc:  2000              # e
    fwc_serial: 10000       # e
    vg:   1.62e-10          # cm^2
    svg:  1.62e-10          # cm^2
    t:    9.4722e-04        # s
    st:   9.4722e-04        # s

pipeline:
  # -> photon
  photon_generation:
    - name: load_image
      func: pyxel.models.photon_generation.load_image
      enabled: true
      arguments:
        image_file: data/Pleiades_HST.fits
        fit_image_to_det: true
        convert_to_photons: true

    - name: shot_noise
      func: pyxel.models.photon_generation.shot_noise
      enabled: true

  # photon -> photon
  optics:
    - name: optical_psf
      func: pyxel.models.optics.optical_psf
      enabled: true
      arguments:
        fov_arcsec: 5 # FOV in arcseconds
        pixelscale: 0.01 #arcsec/pixel
        wavelength: 0.6e-6 # wavelength in meters
        optical_system:
          - item: CircularAperture
            radius: 3.0

    - name: alignment
      func: pyxel.models.optics.alignment
      enabled: true

  # photon -> charge
  charge_generation:
    - name: photoelectrons
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true
      
    - name: tars
      func: pyxel.models.charge_generation.run_tars
      enabled: true
      arguments:
        simulation_mode: cosmic_ray
        running_mode: stepsize
        particle_type: proton
        initial_energy: 100.          # MeV
        particle_number: 100
        incident_angles:
        starting_position:
        spectrum_file: 'data/proton_L2_solarMax_11mm_Shielding.txt'
        random_seed: 4321

  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true

    - name: full_well
      func: pyxel.models.charge_collection.simple_full_well
      enabled: true

  # pixel -> pixel
  charge_transfer:
    - name: cdm
      func: pyxel.models.charge_transfer.cdm
      enabled: true
      arguments:
        parallel_cti: true
        serial_cti: false
        charge_injection: false
        beta_p: 0.3
        tr_p: [3.e-2]
        nt_p: [20.]
        sigma_p: [1.e-10]
        beta_s: 0.
        tr_s: []
        nt_s: []
        sigma_s: []

  # pixel -> signal
  charge_measurement:
    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true

  # signal -> image
  readout_electronics:
    - name: simple_amplifier
      func: pyxel.models.readout_electronics.simple_amplifier
      enabled: true
    - name: simple_digitization
      func: pyxel.models.readout_electronics.simple_processing
      enabled: true
